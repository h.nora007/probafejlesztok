﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public float forceJump = 1000f;                                         // ugróerő

    public void OnCollisionEnter2D(Collision2D collision)           // ütközés
    {
        if (collision.relativeVelocity.y < 0)                       
        {
            Doodle.instance.DoodleRigid.velocity = Vector2.up * forceJump; 
        }
    }

}
