﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerate : MonoBehaviour
{
    public GameObject platformPrefab;       // előregyártott

    void Start()
    {
        Vector3 SpawnerPosition = new Vector3();    // új vektort adunk meg
        
        for (int i = 0; i < 1000; i++)               
        {
            SpawnerPosition.x = Random.Range(-1f, 1f);  // x és y tengely pozíciója
            SpawnerPosition.y += Random.Range(3.5f, 3.5f);      

            Instantiate(platformPrefab, SpawnerPosition, Quaternion.identity);  
        }
    }

}
