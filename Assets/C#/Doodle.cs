﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Doodle : MonoBehaviour
{
    
    public static Doodle instance;                          // script változóit más scriptekben is felhasználhatjuk

    float horizontal;                                       // gyorsulás
    float movement;
    public Rigidbody2D DoodleRigid;

    private float topScore = 0.0f;
    public Text scoreText;
     

    void Start()
    {
        if (instance == null)                               
        {
            instance = this;
        }
    }

    void Update()
    {
        movement = Input.GetAxis("Horizontal");     //írányításhoz kell

        //pontozas
        if(DoodleRigid.velocity.y > 0 && transform.position.y > topScore)
        {
            topScore = transform.position.y;
        }

        scoreText.text = "Pont: " + Mathf.Round(topScore).ToString();
    }

    void FixedUpdate()
    {
        if (Application.platform == RuntimePlatform.Android)    // ha andriod a platform
        {
            horizontal = Input.acceleration.x;                  // x tengely mentén csatlakoztatja a gyorsulást
        }
        else
        {
            horizontal = movement;
        }

        DoodleRigid.velocity = new Vector2(horizontal * 8f, DoodleRigid.velocity.y);     // gyorsulás nagysága
    }

    public void OnCollisionEnter2D(Collision2D collision)       // ütközés
    {
        if (collision.collider.name == "GameOver")              // vége a játéknak
        {
            SceneManager.LoadScene("GameOver");                          // újrainditas
        }
    }

}
