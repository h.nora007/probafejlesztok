﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    public void OnCollisionEnter2D(Collision2D collision)           
    {
        if (collision.relativeVelocity.x < 0)
        {
            Doodle.instance.DoodleRigid.velocity = Vector2.up;
        }
    }

}
